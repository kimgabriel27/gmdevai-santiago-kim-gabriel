﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    Vector3 targetPos;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        float ZAxis = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        float XAxis = Input.GetAxis("Horizontal") * speed * Time.deltaTime;

        transform.Translate(XAxis, 0, ZAxis);

    }
    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        Ray mouseRay = Camera.main.ScreenPointToRay(mousePos);
        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
        RaycastHit hit;
   
        if(Physics.Raycast(mouseRay,out hit, 100))
        {
            targetPos = new Vector3(hit.point.x, 0f, hit.point.z);
        }
        if(Vector3.Distance(targetPos,transform.position) >=0.5f)
        {
            transform.LookAt(targetPos);
        }

    }
  
}

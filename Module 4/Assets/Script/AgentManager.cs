﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentManager : MonoBehaviour
{
    GameObject player;
    GameObject[] agents;
    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("AI");
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 playerPos = player.transform.position;
        //if(Input.GetMouseButtonDown(0))
        //{
        //    RaycastHit hit;

        //    if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000))
        //    {
        //        foreach (GameObject ai in agents)
        //        {
        //            ai.GetComponent<AIControl>().agents.SetDestination(playerPos);
        //        }
        //    }
        //}
        foreach (GameObject ai in agents)
        {
            ai.GetComponent<AIControl>().agents.SetDestination(playerPos);
        }
    }
}
